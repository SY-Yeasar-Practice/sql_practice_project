import {
    Router   
} from "express"
import {
    createNewCourse,
    insertMaterial
} from "../controller/OneToMany_B"

import {createNewTeacher} from "../controller/OneToMany_B_Again"
const route = Router ();

route.get ("/create/bidirectional",createNewCourse)
route.get ("/insert/material",insertMaterial)
// route.get ("/create/material",createNewMaterial)
// route.get ("/delete/course",deleteCourseById)
route.get ("/done",createNewTeacher)

export default route