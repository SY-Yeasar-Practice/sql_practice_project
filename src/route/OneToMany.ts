import {
    Router   
} from "express"
import {
    createNewCourse,
    createNewMaterial,
    deleteCourseById
} from "../controller/OneToMany"

const route = Router ();

route.get ("/create",createNewCourse)
route.get ("/create/material",createNewMaterial)
route.get ("/delete/course",deleteCourseById)

export default route