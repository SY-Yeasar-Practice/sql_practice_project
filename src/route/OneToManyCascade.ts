import {
    Router   
} from "express"
import {
    createNewAuthor,
    addNewBook
} from "../controller/OneToManyCascade"

const route = Router ();

route.get ("/create", createNewAuthor)
route.get ("/create/book", addNewBook)

export default route