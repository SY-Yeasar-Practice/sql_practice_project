import {
    Router   
} from "express"
import {
    createNewUser
} from "../controller/OneToOneUniDirectional"

const route = Router ();

route.get ("/create",createNewUser)

export default route