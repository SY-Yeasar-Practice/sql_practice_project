import {
    Router   
} from "express"
import {
    createNewStudent,
    createNewPaper,
    printWhoHavePaper,
    printWhoDontHavePaper
} from "../../controller/Task/task"

const route = Router ();

route.get ("/create",createNewStudent)
route.post ("/create/paper/:id",createNewPaper)
route.get ("/get/paper",printWhoHavePaper)
route.get ("/get/paper/dontHave",printWhoDontHavePaper)



export default route