import {
    Router   
} from "express"
import {
    createNewCompany,
    createNewProduct,
    deleteCompany
} from "../controller/ManyToMany"

const route = Router ();

route.get ("/create",createNewCompany)
route.get ("/create/product",createNewProduct)
route.get ("/delete",deleteCompany)


export default route