import { BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import {
    Company
} from "./Company"

@Entity()
export class Product extends BaseEntity {
    @PrimaryGeneratedColumn()
    productID?: number
    
    @Column()
    name!: string

    @ManyToMany(() => Company, (company) => company.products,{
        
    })
    @JoinTable({
        name: "product_history",
        joinColumn: {
            name: ,
            referencedColumnName: ""
        }
    })
    companies!:  Company[] 
}