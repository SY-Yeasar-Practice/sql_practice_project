import { BaseEntity, Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import {Product} from "./Product"

@Entity()
export class Company extends BaseEntity {
    @PrimaryGeneratedColumn()
    companyID!: number
    
    @Column()
    name!: string

    @ManyToMany(() => Product, (product) => product.productID, {
        onDelete: "CASCADE"
    })
    products!: Product[]
}