import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm"
import {
    User_Info
} from "./User_Info"

@Entity({
    name: "student"
})
export class Profile extends BaseEntity {
    @PrimaryGeneratedColumn()
    studentID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    class?: string

    @Column({
        type: "varchar",
        length: 25
    })
    result?: string

    @OneToOne (() => User_Info, (user) => user.profileId)
    userId?: User_Info
}
