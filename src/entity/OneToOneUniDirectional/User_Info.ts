import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    JoinColumn,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm"
import {
    Profile
} from "./Profile"

@Entity({
    name: "user"
})
export class User_Info extends BaseEntity {
    @PrimaryGeneratedColumn()
    userId?: number

    @Column({
        type: "varchar",
        length: 25
    })
    firstName?: string

    @Column({
        type: "varchar",
        length: 25
    })
    lastName?: string

    @Column({
        type: "varchar",
        length: 25
    })
    userType ?: string

    @OneToOne (() => Profile,(student) => student.userId,{
        cascade: true
    })
    @JoinColumn ({
        name: "profileId"
    })
    profileId?: Profile
    
}
