import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm"

@Entity({
    name: "course"
})
export class Course extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: "int"
    })
    courseID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    courseName?: string
}

