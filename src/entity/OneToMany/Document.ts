import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn
} from "typeorm"
import {
    Course
} from "./Course"

@Entity({
    name: "document"
})
export class Document extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: "int"
    })
    docID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    title?: string

    @Column({
        type: "varchar",
        length: 25
    })
    content?: string

    @ManyToOne(() => Course, {
        onDelete: "CASCADE",
        nullable: true
    })
    @JoinColumn({
        name: "courseFor"
    })
    courseFor?: Course | null
}
