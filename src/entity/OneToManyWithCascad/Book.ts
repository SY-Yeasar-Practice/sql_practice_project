import {
    Entity,
    PrimaryGeneratedColumn,
    BaseEntity,
    Column,
    ManyToOne,
    JoinColumn
} from "typeorm"
import {Author} from  "./Author"

// @Entity({name: "book"})
// export class Book extends BaseEntity {
//     @PrimaryGeneratedColumn({
//         type: "int",
//         name: "bookID"
//     })
//     public bookID!:  number

//     @Column({name: "name"})
//     public name!: string

//     @ManyToOne(() => Author, (author) => author.books, {
//         onDelete: "CASCADE"
//     })
//     @JoinColumn({name: "author"})
//     author!: Author 
// }

@Entity()
export class Book extends BaseEntity {
    @PrimaryGeneratedColumn()
    bookID?: number
    @ManyToOne(() => Author, (author) => author.books, {
        onDelete: 'CASCADE'
    })
    public author?: Author
}