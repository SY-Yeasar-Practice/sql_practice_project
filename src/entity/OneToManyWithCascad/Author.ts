import {
    Entity,
    PrimaryGeneratedColumn,
    BaseEntity,
    Column,
    OneToMany
} from "typeorm"
import {Book}from "./Book"

// @Entity({name: "author"})
// export class Author extends BaseEntity {
//     @PrimaryGeneratedColumn({
//         type: "int",
//         name: "authorID"
//     })
//     public authorID!:  number

//     @Column({name: "name"})
//     public name!: string

//     @OneToMany(() => Book, (book) => book.author, {
//         cascade: true
//     })
//     books?: Book [] | null
// }
@Entity()
export class Author extends BaseEntity {
    @PrimaryGeneratedColumn()
    authorID?: number
    @OneToMany(() => Book, (book) => book.author, {
        cascade: true
    })
    public books?: Book[];
}
