import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    JoinColumn,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm"
import {
    Student
} from "./Student"

@Entity({
    name: "user"
})
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    userId?: number

    @Column({
        type: "varchar",
        length: 25
    })
    firstName?: string

    @Column({
        type: "varchar",
        length: 25
    })
    lastName?: string

    @Column({
        type: "varchar",
        length: 25
    })
    userType ?: string

    @OneToOne (() => Student,{
        cascade: true
    })
    @JoinColumn ({
        name: "profileId"
    })
    profileId?: Student
    
}
