import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm"

@Entity({
    name: "student"
})
export class Student extends BaseEntity {
    @PrimaryGeneratedColumn()
    studentID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    class?: string

    @Column({
        type: "varchar",
        length: 25
    })
    result?: string
}
