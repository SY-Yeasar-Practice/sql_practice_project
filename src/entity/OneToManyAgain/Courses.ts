import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn
} from "typeorm"
import {
    Teacher
} from "./Teacher"

@Entity({
    name: "courses"
})
export class Courses extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: "int"
    })
    courseID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    courseName?: string

    @ManyToOne (() => Teacher, (teacher) => teacher.courses, {
        onDelete: "CASCADE"
    } )
    @JoinColumn ({name: "instructor"})
    instructor?: Teacher 

    
}

