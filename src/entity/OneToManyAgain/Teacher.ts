import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToMany
} from "typeorm"
import { Course } from "../OneToMany/Course"
import {Courses} from "./Courses"

@Entity({
    name: "teachers"
})
export class Teacher extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: "int"
    })
    teacherID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    name?: string

    @OneToMany (() => Courses, (course) => course.instructor, {
        cascade: true
    })
    courses?: Courses[]
}

