import {
    Column,
    Entity, OneToMany, OneToOne, PrimaryGeneratedColumn, BaseEntity
} from "typeorm"
import {Paper} from "./Paper"

@Entity({name: "student"})
export class Student extends BaseEntity{
    @PrimaryGeneratedColumn({
        name: "studentId"
    })
    studentId!: number 

    @Column({
        name: "firstName"
    })
    firstName!: string

    @OneToMany(() => Paper, (paper) => paper.studentId)
    papers!: Paper[]
}