import {
    Column,
    Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BaseEntity
} from "typeorm"
import {Student} from "./Student"

@Entity({name: "paper"})
export class Paper extends BaseEntity{
    @PrimaryGeneratedColumn({
        name: "paperId"
    })
    paperId!: number 

    @Column({
        name: "title"
    })
    title!: string

    @Column({
        name: "grade"
    })
    grade!: string

    @ManyToOne(() => Student, (student) => student.papers, {
        onDelete: "CASCADE"
    })
    @JoinColumn({
        name: "studentId"
    })
    studentId!: Student
}