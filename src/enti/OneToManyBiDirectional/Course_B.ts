import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    OneToMany
} from "typeorm"
import {
    Document_B
} from  "./Document_B"


@Entity({
    name: "course_B"
})
export class Course_B extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: "int"
    })
    courseID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    courseName?: string

    @OneToMany(() => Document_B, (document) => document.courseFor, {
        eager: true
    })
    documents?: Document_B[] | null
}

