import {
    Entity,
    BaseEntity,
    Column,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn
} from "typeorm"
import {
    Course_B
} from "./Course_B"

@Entity({
    name: "document_B"
})
export class Document_B extends BaseEntity {
    @PrimaryGeneratedColumn({
        type: "int"
    })
    docID?: number

    @Column({
        type: "varchar",
        length: 25
    })
    title?: string

    @Column({
        type: "varchar",
        length: 25
    })
    content?: string

    @ManyToOne(() => Course_B,(course) => course.documents, {
        onDelete: "CASCADE",
        nullable: true
    })
    @JoinColumn({
        name: "courseFor"
    })
    courseFor?: Course_B | null
}
