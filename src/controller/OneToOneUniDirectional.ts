import {
    Express,
    Request,
    Response
} from "express"
import {Student}from "../entity/OneToOne/Student"
import {User}from "../entity/OneToOne/User"

type Body = (req: Request, res: Response) => Promise<void>

const createNewUser:Body = async (req, res) => {
    try {
        const newUser = new User ();
        newUser.firstName = "MD"
        newUser.lastName = "Shopnil"
        newUser.userType = "student"
        const saveUser = await newUser.save();

        const newProfile = new Student();
        newProfile.class = "seven"
        newProfile.result = "A"
        const saveProfile = await newProfile.save();

        const findUser:User | null = await User.findOne ({
            where: {
                userId: saveUser.userId
            }
        })
        const isUpdate = await  User.update (
            {
                userId: findUser?.userId
            },
            {
                profileId : saveProfile
            }
        )
        if (isUpdate) {
            res.json ({
                message: "Data updated"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

export {
    createNewUser
}