import {
    Express,
    Request,
    Response
} from "express"
import {Author}from "../model/OneToManyWithCascad/Author"
import {Book}from "../model/OneToManyWithCascad/Book"


type Body = (req: Request, res: Response) => Promise<void>

const createNewAuthor:Body = async (req, res) => {
    try {
        const newAuthor = new Author ();
        const isSave = await newAuthor.save();
        if (isSave) {
            res.json ({
                message: "New Author saved"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 


const addNewBook:Body = async (req, res) => {
    try {
        // const newBook = new Book ();
        // newBook.name = "Book One"
        // const findAuthor:Author | null = await Author.findOne({
        //     where: {
        //         authorID: 1
        //     }
            
        // })
        // console.log(findAuthor)
        // findAuthor.books.push(newBook)
        // const isUpdate = await findAuthor?.save()
        // if (isUpdate) {
        //     console.log(isUpdate)
        //     res.json ({
        //         message: "New Book save"
        //     })
        // }
        const createNewBook = new Book()
        await createNewBook.save()
        const author = await Author.findOne({where: {authorID: 1} });
        author?.books?.push(createNewBook)
        const a = await author?.save()
        if (a) {
            console.log(a)
            res.json ({
                message: "Saved"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 


export {
    createNewAuthor,
    addNewBook
}