import {
    Express,
    Request,
    Response
} from "express"
import {Course}from "../entity/OneToMany/Course"
import {Document}from "../entity/OneToMany/Document"

type Body = (req: Request, res: Response) => Promise<void>

const createNewCourse:Body = async (req, res) => {
    try {
        const newCourseInstance = new Course ();
        newCourseInstance.courseName = "CSE203"
        const saveCourse = await newCourseInstance.save();
        if (saveCourse) {
            res.json ({
                message: "New Course Created"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

const createNewMaterial:Body = async (req, res) => {
    try {
        const findCourse:Course | null = await Course.findOne ({
            where : {
                courseID: 1
            }
        })
        const newDoc = new Document ();
        newDoc.content = "Hello World"
        newDoc.title = "Document One"
        newDoc.courseFor =  findCourse
        const saveCourse = await newDoc.save();
        if (saveCourse) {
            res.json ({
                message: "New Material Created"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

const deleteCourseById:Body = async (req, res) => {
    try {
        const deleteCourse= await Course.delete ({
            courseID: 1
        })
        if (deleteCourse) {
            res.json ({
                message: "Course Successfully deleted"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

export {
    createNewCourse,
    createNewMaterial,
    deleteCourseById
}