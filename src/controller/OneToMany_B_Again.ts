import {
    Express,
    Request,
    Response
} from "express"
import { Course } from "../entity/OneToMany/Course";
import { Courses } from "../entity/OneToManyAgain/Courses";
import { Teacher } from "../entity/OneToManyAgain/Teacher"
import {getManager} from  "typeorm"


type Body = (req: Request, res: Response) => Promise<void>

const createNewTeacher:Body = async (req, res) => {
    try {
        // const newTeacher = new Teacher ();
        // newTeacher.name = "Nirmal"
        // const isSave = await newTeacher.save();
        // if (isSave) {
        //     res.json ({
        //         message: "New Teacher saved"
        //     })
        // }


        // const newCourseOne = new Courses ();
        // newCourseOne.courseName = "ENV212"
        // const isSave = await newCourseOne.save();
        // if (isSave) {
        //     res.json ({
        //         message: "New Course create"
        //     })
        // }
        // const newCourse = new Courses()
        // newCourse.courseName = "CSE301"
        // const saveCourse = await newCourse.save()
        // const findTeacher = await Teacher.findOne (
        //     { 
        //         where: {
        //             teacherID: 1
        //         }
        //     }
        // )
        // findTeacher?.courses?.push (saveCourse)
        // const isSave = await findTeacher?.save();
        // if (isSave) {
        //     // console.log(isSave)
        //     res.json ({
        //         message: "New Course Add"
        //     })
        // }
        const entityManager = getManager();

        const findAllCourse = await entityManager.query (
            `
                SELECT * FROM TEACHERS
                LEFT JOIN COURSES 
                ON COURSES.INSTRUCTOR = TEACHERS.TEACHERID
            `
        )
        if (findAllCourse) {
            res.json ({
                message: "Data found",
                data: findAllCourse
            })
        } 
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

// const insertMaterial:Body = async (req, res) => {
//     try {
//         const newDocOne = new Document_B ();
//         newDocOne.content = "Hello World One"
//         newDocOne.title = "Document One"
//         const DocOne = await newDocOne.save()

//         const newDocTwo = new Document_B ();
//         newDocTwo.content = "Hello World Two"
//         newDocTwo.title = "Document Two"
//         const DocTwo = await  newDocTwo.save()

//         const updateCourse = await Course_B.update (
//             {
//                 courseID: 1
//             },
//             {
//                 documents: [DocTwo, DocOne]
//             }
//         )
//         console.log(updateCourse)
//         if (updateCourse) {
//             res.json ({
//                 message: "New Material Inserted"
//             })
//         }
//     }catch(err) {
//         console.log(err)
//         res.json ({
//             message: err
//         })
//     }
// } 

// // const deleteCourseById:Body = async (req, res) => {
// //     try {
// //         const deleteCourse= await Course.delete ({
// //             courseID: 1
// //         })
// //         if (deleteCourse) {
// //             res.json ({
// //                 message: "Course Successfully deleted"
// //             })
// //         }
// //     }catch(err) {
// //         console.log(err)
// //         res.json ({
// //             message: err
// //         })
// //     }
// // } 

export {
    createNewTeacher
}