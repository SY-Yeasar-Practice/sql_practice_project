import {
    Express,
    Request,
    Response
} from "express"
import {Company}from "../entity/ManyToMany/Company"
import {Product}from "../entity/ManyToMany/Product"


type Body = (req: Request, res: Response) => Promise<void>

const createNewCompany:Body = async (req, res) => {
    try {
        const newCompany = new Company ();
        newCompany.name = "Pran group"
        const saveCompany = await newCompany.save();
        if (saveCompany) {
            res.json ({
                message: "New Company has created"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

const createNewProduct:Body = async (req, res) => {
    try {
        const findCompany = await Company.findOne (
            {
                where: {
                    companyID: 1
                }
            }
        )
        const company: Company = await findCompany?.save()!
        const chair = new Product ();
        chair.name = "chair"
        chair.companies = [company]!
        await chair.save();
        
        const table = new Product ();
        table.name = "table"
        table.companies = [company]!
        await table.save();

        res.json ({
            message: "New Product add"
        })

        
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

const deleteCompany:Body = async (req, res) => {
    try {
        const deleteCompany= await Company.delete ({
            companyID: 1
        })
        if (deleteCompany) {
            res.json ({
                message: "Company Successfully deleted"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

export {
    createNewCompany,
    createNewProduct,
    deleteCompany
}