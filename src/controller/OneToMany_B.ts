import {
    Express,
    Request,
    Response
} from "express"
import {Course_B}from "../enti/OneToManyBiDirectional/Course_B"
import {Document_B}from "../enti/OneToManyBiDirectional/Document_B"

type Body = (req: Request, res: Response) => Promise<void>

const createNewCourse:Body = async (req, res) => {
    try {
        const newCourseInstance = new Course_B ();
        newCourseInstance.courseName = "CSE301"
        const saveCourse = await newCourseInstance.save();
        if (saveCourse) {
            res.json ({
                message: "New Course Created"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

const insertMaterial:Body = async (req, res) => {
    try {
        const newDocOne = new Document_B ();
        newDocOne.content = "Hello World One"
        newDocOne.title = "Document One"
        const DocOne = await newDocOne.save()

        const newDocTwo = new Document_B ();
        newDocTwo.content = "Hello World Two"
        newDocTwo.title = "Document Two"
        const DocTwo = await  newDocTwo.save()

        const updateCourse = await Course_B.update (
            {
                courseID: 1
            },
            {
                documents: [DocTwo, DocOne]
            }
        )
        console.log(updateCourse)
        if (updateCourse) {
            res.json ({
                message: "New Material Inserted"
            })
        }
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

// const deleteCourseById:Body = async (req, res) => {
//     try {
//         const deleteCourse= await Course.delete ({
//             courseID: 1
//         })
//         if (deleteCourse) {
//             res.json ({
//                 message: "Course Successfully deleted"
//             })
//         }
//     }catch(err) {
//         console.log(err)
//         res.json ({
//             message: err
//         })
//     }
// } 

export {
    createNewCourse,
    insertMaterial,
    // deleteCourseById
}