import e, {
    Express,
    Request,
    Response
} from "express"
import {Student}from "../../model/Task/Student"
import {Paper}from "../../model/Task/Paper"
import {
    DataSource,
    getManager,
} from "typeorm"


type Body = (req: Request, res: Response) => Promise<void>

const createNewStudent:Body = async (req, res) => {
    try {
        const entityManager = getManager()
        const newStudent = await entityManager.insert (
            Student,
            [
                {
                    firstName: "Caleb"
                },
                {
                    firstName: "Samantha"
                },
                {
                    firstName: "Raj"
                },
                {
                    firstName: "Carlos"
                },
                {
                    firstName: "Lisa"
                }
            ]
        )
        if (newStudent) {
            res.json({
                message: "All Student Create successfully"
            })
        }
        
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

const createNewPaper:Body = async (req, res) => {
    try {
        const existingStudent = await Student
                            .createQueryBuilder("student")
                            .where("STUDENT.STUDENTID = :studentID", {studentID: req.params.id})
                            .getOne()
        if (existingStudent) {
            const newPaper = await new Paper();
            newPaper.title = req.body.title
            newPaper.grade = req.body.grade
            newPaper.studentId = existingStudent!
            const saveNewPaper = await newPaper.save();
            if (saveNewPaper) {
                res.json ({
                    message: "A new Paper inserted"
                })
            }else {
                res.json ({
                    message: "Paper Insert Failed"
                })
            }
        }else {
            res.json({
                message: "Student not found"
            })
        }

    
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
} 

//print those student first and last name who have paper s
const printWhoHavePaper:Body = async (req, res) => {
     try {
        const findWhoHavePaper = await Student.createQueryBuilder("student")
                                                .innerJoin(
                                                    "student.papers", "paper"
                                                )
                                                .select(["paper.grade", "student.firstName", "paper.title"])
                                                .getMany()
        res.send(findWhoHavePaper)
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
}

//print those student first and last name who does not shave paper
const printWhoDontHavePaper:Body = async (req, res) => {
     try {
        const findWhoDontHavePaper = await Student.createQueryBuilder("student")
                                                    .leftJoin ("student.papers", "paper")
                                                    .where ("student.id = ")
                                                    .getMany()
                                                
        res.send(findWhoDontHavePaper)
    }catch(err) {
        console.log(err)
        res.json ({
            message: err
        })
    }
}
export {
    createNewStudent,
    createNewPaper,
    printWhoHavePaper,
    printWhoDontHavePaper
}