import express,{Application} from "express"
require("dotenv").config()
import {createConnection, DataSource, DataSourceOptions} from "typeorm"
// import oneToOneRoute from "./src/route/OneToOne"
// import oneToOneUniDirectionalRoute from "./src/route/OneToOne"
// import oneToManyRoute from "./src/route/OneToMany"
// import oneToManyRouteBiDirectional from "./src/route/OneToMany_B"
// import oneToManyCascadeRoute from "./src/route/OneToManyCascade"
// import manyToManyBiDirectional from "./src/route/ManyToMany"
import taskOneRoute from "./src/route/task/task"


const app:Application = express();


//dot env file 
const port:number | string = process.env.PORT || 8080

app.use (express.json({limit: "250mb"}))
app.use (express.urlencoded({extended: true, limit: "250mb"}))


// connect the my sql database 
createConnection ().then (() => {
    console.log(`Server is connected to database`)
}).catch((err) => {
    console.log(err)
})


//create  the server 
app.listen (port, ():void => {
    console.log(`Server is connected to the ${port}`)
})


// app.use ("/oneToOne", oneToOneRoute)
// app.use ("/oneToOneUni", oneToOneUniDirectionalRoute)
// app.use ("/oneToManyRoute", oneToManyRoute)
// app.use ("/oneToManyBiRoute", oneToManyRouteBiDirectional)
// app.use ("/author", oneToManyCascadeRoute)
// app.use ("/company", manyToManyBiDirectional)
app.use ("/student", taskOneRoute)